WindowView
==========

An Android ImageView that can be panned around by tilting your device,
as if you were looking through a window.